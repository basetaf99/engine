add_library_ex(CommonDataModelTests
  H_FILES
    CommonDataModelTest.h
  CPP_FILES
    ActionTest.cpp
    AdvancedCircuitTest.cpp
    BasicCircuitTest.cpp
    BlackBoxTest.cpp
    CommonDataModelTest.cpp
    ConvertScenarioLogs.cpp
    GasCompartmentTest.cpp
    LiquidCompartmentTest.cpp
    ReadPatientDirectory.cpp
    ReadSubstanceDirectory.cpp
    ScalarTest.cpp
    SubstanceTransport.cpp
    ThermalCompartmentTest.cpp
    TissueCompartmentTest.cpp
    UnitsTest.cpp
    WaveformInterpolatorTest.cpp
  PUBLIC_DEPENDS
    CommonDataModel
  LIB_INSTALL_ONLY
)
